import ShoppingCart from "../../components/shopping-cart";
import Layout from "../../layouts/main";

const Products = () => (
    <Layout>
        <ShoppingCart />
    </Layout>
);

export default Products;
