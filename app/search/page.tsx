"use client";
import { BackButton } from "@/components/atoms/bottom/BackButton";
import SeachBox from "@/components/molecules/SearchBox";
import Categories from "@/components/pages/search/Categories";
import FilterBox from "@/components/pages/search/FilterBox";
import SearchResult from "@/components/pages/search/SearchResult";
import SpecialFood from "@/components/pages/search/SpecialFood";
import SpecialRestaurants from "@/components/pages/search/SpecialRestaurants";
import { useFoodDiscovery } from "@/hooks/useFoodDecovery";
import { Flex } from "@chakra-ui/react";

const Search = () => {
    const {
        keySearch,
        searchResult,
        isShowFilterBox,
        filterCondition,

        onSearch,
        onChangeValue,
        onChangeFilterOptions,
    } = useFoodDiscovery();
    const onKeyDown: React.KeyboardEventHandler<HTMLInputElement> = (e) => {
        if (e.key === "Enter") onSearch();
    };
    return (
        <Flex flexDirection={"column"} alignItems={"center"} bg="white" w="100%" h="100%">
            <Flex flexDirection={"column"} alignItems={"flex-start"} py="2rem" px="6.7rem" w="100%">
                <BackButton label="Quay lại trang trước" />
                <SeachBox
                    placeholder="Gõ tên món ăn, nhà hàng mà bạn đang muốn tìm"
                    flex="1"
                    groupsProps={{ my: "1rem" }}
                    value={keySearch}
                    onChange={(e) => {
                        onChangeValue<string>("keySearch", e.target.value ?? "");
                    }}
                    onKeyDown={onKeyDown}
                />
                {isShowFilterBox ? (
                    <>
                        <FilterBox condition={filterCondition} onChangeFilterOptions={onChangeFilterOptions} />
                        <SearchResult type={filterCondition.type} result={searchResult} />
                    </>
                ) : (
                    <>
                        <Categories />
                        <SpecialFood />
                        <SpecialRestaurants />
                    </>
                )}
            </Flex>
        </Flex>
    );
};

export default Search;
