export enum FilterType {
    Food = "food",
    Restaurant = "restaurant",
}

export enum SortOrder {
    ASC = "asc",
    DESC = "desc",
}
